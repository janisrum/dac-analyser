
import scala.sys.process._

/**
  * Created by janis on 14/10/2016.
  */
object DotHelper {

  // generate DOT string from Execution Plan content
  def getExecutionPlanDot(execPlan: String, command: String, executionPlanContent: List[Map[String,String]]): String = {

    val tasksByDepth: Map[String, List[Map[String, String]]] = executionPlanContent groupBy (_("TASK_DEPTH"))
    val depths: List[Int] = tasksByDepth.keys.toList map (_.toInt) sorted
    val depthsInDot: String = (depths mkString(" -> ")) + "[arrowhead=none]"

    val tasksGroupedByDepthInDot: String = (depths
      map (depth => tasksByDepth(depth.toString))
      map (tasksAtCurrentDepth => tasksAtCurrentDepth
      filter (task => task(command) != "") // if a task only has a command for Incremental Load, the Full Load command will be empty and needs to be removed
      map (task => s"{rank=same; ${task("TASK_DEPTH")}; ${formatTaskInDot(command,task)};}" )
      mkString("\n\t")
      )
      mkString("\n\n\t")
      )

    val executionPlanInDot: String =
      s"digraph EP_${execPlan.replaceAll("""[ -.\(\)\\/]""","_")} {\n" +
        //s"digraph EP_${executionPlanName.replace(" ","_").replace("-","_").replace(".","_")} {\n" +
        "\tgraph [nodesep=0.1, ranksep=0.25]\n" +
        "\tnode [shape=none, fontname=\"arial\"; fontsize=11]\n" +
        s"\t${depthsInDot}\n\n" +
        "\tnode [shape=box; style=filled; color=\"#BCDC7C\"; fontname=\"arial\"; fontsize=11]\n\n" +
        s"\t$tasksGroupedByDepthInDot\n" +
        "}"

    executionPlanInDot
  }

  def convertFileDotToSvg(graphvizBinRoot: String, dotFileRoot: String, dotFileName: String, svgFileRoot: String, svgFileName: String): Unit = {
    val command = s"""${graphvizBinRoot}\\dot.exe -Tsvg "${dotFileRoot}\\${dotFileName}" -o "${svgFileRoot}\\${svgFileName}" """
    command ! // sys.process
  }

  def formatTaskInDot(command: String, task: Map[String,String]): String = {
    "\"" + task(command) + "\"" +
      (
        // Informatica Task node green colour is set as the default. These two are overrides:
        if      (task("EXEC_TYPE") == "Stored Procedure") """[color="#88AAE3";]"""
        else if (task("EXEC_TYPE") == "External Program") """[color="#E78383";]"""
        else ""
        )
  }
  
}
