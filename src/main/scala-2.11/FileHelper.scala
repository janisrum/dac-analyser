import java.io.{File, PrintWriter}

import scala.io.Source
import scala.util.parsing.json.JSON

/**
  * Created by janis on 14/10/2016.
  */
object FileHelper {

  // uses wrapper file template to create a wrapper file for an Execution Plan
  def writeExecPlanHtml(htmlTemplatePath: String, targetWriteFolder: String, execPlanAttributes: Map[String,String]): Unit = {
    val wrapperPageTemplate: String = io.Source.fromFile(htmlTemplatePath).getLines().mkString("\n")
    val execPlanName = execPlanAttributes("[executionPlanName]")
    val wrapperFileName = "Page__" + execPlanName + ".html"
    val wrapperFileContent = replaceFromMap(wrapperPageTemplate, execPlanAttributes)

    writeFile(targetWriteFolder, wrapperFileName, wrapperFileContent)
  }

  def writeFile(root: String, fileName: String, content: String): Unit = {
    val pw = new PrintWriter(new File(s"$root\\$fileName" ))
    pw.write(content)
    pw.close
  }

  def readCsvFile(fileName: String): CsvContent = {
    val fileSource = Source.fromFile(fileName)
    val csvLines = fileSource.getLines

    val header = csvLines.next().split(",") map (_.replace("\"","")) toList
    val values = csvLines.toList map (_.split(",") map (_.replace("\"","")) )
    val valuesMaps = values map (header zip _ toMap)

    fileSource.close
    CsvContent(fileName, header, valuesMaps)
  }

  def replaceFromMap(sourceText: String, replaceDict: Map[String,String]): String = replaceDict.foldLeft (sourceText)((accu: String, n:(String,String)) => accu.replace(n._1, n._2) )

  def configFromJson(jsonRoot: String): Map[String,String] = {
    val jsonText = scala.io.Source.fromFile(jsonRoot).getLines().mkString("\n")
    JSON.parseFull(jsonText).get.asInstanceOf[Map[String,String]]
  }
}
