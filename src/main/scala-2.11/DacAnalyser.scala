

object DacAnalyser  {

  def main(args: Array[String]): Unit = {

    val paths = FileHelper.configFromJson("""config.json""")

    // this tool requires GraphViz to be installed on the maching where it is executed. Download GraphViz from here: http://www.graphviz.org/Download.php
    val graphvizBinRoot = paths("graphvizBinRoot") // """C:\Program Files (x86)\Graphviz2.38\bin"""

    // template
    val templateHtmlPath = paths("templateHtmlPath") // """src\main\resources\Page__TEMPLATE.html"""

    // source
    val sourceContentFilePath = paths("sourceContentFilePath") // """C:\Developer\Project Source Data\DAC Analyser\Source\DAC_content.csv"""

    // result
    val resultRoot = paths("resultRoot") // """C:\Developer\Project Source Data\DAC Analyser\Result"""
    val resultDetailsRoot = resultRoot + """\ExecutionPlans"""


    // MAIN
    val dacTasks: CsvContent = FileHelper.readCsvFile(sourceContentFilePath)
    val dacTasksByExecPlan = dacTasks.content groupBy (_ ("EXEC_PLAN_NAME"))
    val dacExecPlans = dacTasksByExecPlan.keys

    dacExecPlans foreach (execPlan => {
      println(s"Processing DAC Execution Plan $execPlan...")

      val executionPlanAttributes = Map(
        "[executionPlanName]" -> execPlan,
        "[incrSvgFileName]" -> ("ExecutionPlans/" + svgCreator(execPlan, "INCREMENTAL_COMMAND")),
        "[fullSvgFileName]" -> ("ExecutionPlans/" + svgCreator(execPlan, "FULL_COMMAND"))
      )
      FileHelper.writeExecPlanHtml(templateHtmlPath, resultRoot, executionPlanAttributes)
    })


    def svgCreator(execPlan: String, command: String): String = {
      val filePrefix = if (command == "FULL_COMMAND") "FULL" else "INCR"
      val dotFileName = s"${filePrefix}__${execPlan}.dot"
      val svgFileName = s"${filePrefix}__${execPlan}.svg"

      val dotContentIncremental: String = DotHelper.getExecutionPlanDot(execPlan = execPlan, command = command, executionPlanContent = dacTasksByExecPlan(execPlan))
      FileHelper.writeFile(resultDetailsRoot, dotFileName, dotContentIncremental)
      DotHelper.convertFileDotToSvg(graphvizBinRoot, resultDetailsRoot, dotFileName, resultDetailsRoot, svgFileName)

      svgFileName
    }

    println("Done!")
  }
}
