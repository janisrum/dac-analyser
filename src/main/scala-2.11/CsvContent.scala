/**
  * Created by Janis Rumnieks on 02/08/2016.
  */

case class CsvContent(fileName: String, header: List[String], content: List[Map[String,String]])
