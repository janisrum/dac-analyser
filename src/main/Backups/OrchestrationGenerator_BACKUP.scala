package EtlAnalysis

import java.io.File

import Common.{DotHelper, FileHelper}
import InformaticaDependencies.InformaticaXmlReader

import scala.xml.Elem

/**
  * Created by Janis Rumnieks on 02/08/2016.
  */

object OrchestrationGenerator_BACKUP extends App {

  // ---------------------------------------------------
  // Summary of all DAC content
  val dacContentFilePath = """C:\Developer\Source Data\Dac Analysis\DAC_content.csv"""
  val orchestrationWorkflowsTargetPath = """C:\Developer\Source Data\OrchestrationContent"""
  val orchestrationWorkflowTemplatePath = """C:\Developer\Source Data\OrchestrationContent\Templates\Orchestration__TEMPLATE_3.XML"""


  case class dacContent(executionPlanName: String, taskDepth: Int, folderName: String, taskName: String, execType: String, incrementalCommand: String, fullCommand: String, truncFullLoadFlag: String, truncFlag: String)
  val allDacContent: CsvContent = FileHelper.readCsvFile(dacContentFilePath)

  val allDacTasks = allDacContent.content map
    (row => dacContent(
      executionPlanName = row("EXEC_PLAN_NAME"),
      taskDepth = row("TASK_DEPTH").toInt,
      folderName = row("FOLDER_NAME"),
      taskName = row("TASK_NAME"),
      execType = row("EXEC_TYPE"),
      incrementalCommand = row("INCREMENTAL_COMMAND"),
      fullCommand = row("FULL_COMMAND"),
      truncFullLoadFlag = row("TRUNC_FULLLOAD_FLG"),
      truncFlag = row("TRUNC_FLG")
      )
    )

  val allDacExecutionPlanNames = (allDacTasks map (_.executionPlanName) distinct) sorted
  val usedExecutionPlanNamesScheduled: Set[String] = FileHelper.readTxtFile("""C:\Developer\Source Data\Dac Analysis\DAC_Execution_Plans.txt""")

  val usedExecutionPlanNames = allDacExecutionPlanNames filter (usedExecutionPlanNamesScheduled contains (_))

  // ------ workflows, sessions and links
  val infaWorkflowExportsRoot = """C:\Developer\Source Data\InformaticaExports_Workflows"""
  val infaWorkflowSourceFiles: List[File] = InformaticaXmlReader.filesFromFolder(infaWorkflowExportsRoot)

  val workflowSessionsAndLinks: List[WorkflowSessionsAndLinks] = infaWorkflowSourceFiles flatMap (
    file => {
      println(s"Interpreting Workflow XML from ${file.getPath}...")
      val xmlContent: Elem = InformaticaXmlReader.xmlFileContent(file)
      InformaticaXmlReader.workflowSessionsAndLinksFromXml(xmlContent)
    }
    )
  // ------


  usedExecutionPlanNames foreach (executionPlanName => {
    println(s"Processing DAC Execution Plan $executionPlanName...")

    val execPlanTasks: List[dacContent] = allDacTasks filter (_.executionPlanName == executionPlanName)
    val depths = execPlanTasks.map(_.taskDepth).distinct.sorted

    val incrementalExecPlanWorkflows = execPlanTasks filter (_.execType == "Informatica") /*map (task => task.incrementalCommand)*/ filter (_.incrementalCommand != "")
    val fullExecPlanWorkflows = execPlanTasks filter (_.execType == "Informatica") /*map (task => task.fullCommand)*/ filter (_.fullCommand != "")

    val (incrementalSessionsNotFlat, incrementalWorkflowLinksNotFlat) = (for {
      incrementalWorkflow <- incrementalExecPlanWorkflows
      workflowContent <- workflowSessionsAndLinks if workflowContent.workflowName == incrementalWorkflow.incrementalCommand
      sessions = workflowContent.sessions map (session => (session, incrementalWorkflow.taskDepth))
      links = workflowContent.links map (link => (link, incrementalWorkflow.taskDepth))
    } yield(sessions, links)).unzip

    val incrementalSessions = incrementalSessionsNotFlat.flatten
    val incrementalInternalLinks = incrementalWorkflowLinksNotFlat.flatten filter (link => link._1.fromTask != "Start")

    val incrementalSessionsEntryPoints = incrementalSessions filter (session => ! (incrementalInternalLinks.exists(internalLink => internalLink._1.toTask == session._1)) )
    val incrementalSessionsExitPoints = incrementalSessions filter (session => ! (incrementalInternalLinks.exists(internalLink => internalLink._1.fromTask == session._1)) )

    val orchestrationLinksFromAccumulator = for {
      depth <- depths
      fromAccumulator = if(depth-1 == 0) "Start" else s"Depth_${depth-1}"
      entryPointSession <- incrementalSessionsEntryPoints if entryPointSession._2 == depth
      toTask = entryPointSession._1
    } yield (s"""\t\t<WORKFLOWLINK CONDITION ="" FROMTASK ="$fromAccumulator" TOTASK ="$toTask"/>""")

    val orchestrationLinksToAccumulator = for {
      depth <- depths
      toAccumulator = s"Depth_${depth}"
      exitPointSession <- incrementalSessionsExitPoints if exitPointSession._2 == depth
      fromTask = exitPointSession._1
    } yield (s"""\t\t<WORKFLOWLINK CONDITION ="" FROMTASK ="$fromTask" TOTASK ="$toAccumulator"/>""")

    val orchestrationLinks = "        <!-- Accumulator-to-Task: -->\n" + orchestrationLinksFromAccumulator.mkString("\n") + "\n\n        <!-- Task-to-Accumulator: -->\n" + orchestrationLinksToAccumulator.mkString("\n")

    // depths map (depth => incrementalSessionsEntryPoints filter (entry => entry._2 == depth)   if(depth-1 == 0) "Start" else s"Depth_${depth-1}")


    // todo:
    // 1. add sessions to XML
    // 2. add links between sessions (except Start-to-Session link) to XML
    // 3. construct links between Start->Session or Decision->Session
    // 4. construct links between Session->Decision
    // done


    val orchestrationWorkflowName = "Orchestration__" + executionPlanName

    val sessionTaskInstances = incrementalSessions map ( incrementalSessionName =>
             s"""        <TASKINSTANCE DESCRIPTION ="" FAIL_PARENT_IF_INSTANCE_DID_NOT_RUN ="YES" FAIL_PARENT_IF_INSTANCE_FAILS ="YES" ISENABLED ="YES" NAME ="${incrementalSessionName._1}" REUSABLE ="YES" TASKNAME ="${incrementalSessionName._1}" TASKTYPE ="Session" TREAT_INPUTLINK_AS_AND ="YES"/>"""
      ) mkString("\n")


    // todo: add criteria for success - Decision Name attribute value
    val decisionTasks = depths map (depth => s"""        <TASK DESCRIPTION ="" NAME ="Depth_$depth" REUSABLE ="NO" TYPE ="Decision" VERSIONNUMBER ="1"><ATTRIBUTE NAME ="Decision Name" VALUE =""/></TASK>""") mkString("\n")

    val decisionTaskInstances = depths map (depth => s"""        <TASKINSTANCE DESCRIPTION ="" FAIL_PARENT_IF_INSTANCE_DID_NOT_RUN ="YES" FAIL_PARENT_IF_INSTANCE_FAILS ="YES" ISENABLED ="YES" NAME ="Depth_$depth" REUSABLE ="NO" TASKNAME ="Depth_$depth" TASKTYPE ="Decision" TREAT_INPUTLINK_AS_AND ="YES"/>""") mkString("\n")

    val workflowLinksSourceInternal = incrementalInternalLinks map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link._1.fromTask}" TOTASK ="${link._1.toTask}"/>""") mkString("\n")

    val workflowLinksOrchestration = orchestrationLinks

    val workflowLogFileName = "Orchestration__" + executionPlanName + ".log"


    val executionPlanAttributes = Map(
      "[orchestrationWorkflowName]" -> orchestrationWorkflowName,
      "[sessionTaskInstances]" -> sessionTaskInstances,
      "[decisionTasks]" -> decisionTasks,
      "[decisionTaskInstances]" -> decisionTaskInstances,
      "[workflowLinksSourceInternal]" -> workflowLinksSourceInternal,
      "[workflowLinksOrchestration]" -> workflowLinksOrchestration,
      "[workflowLogFileName]" -> workflowLogFileName
    )

    val orchestrationWorkflowTemplateContent = scala.io.Source.fromFile(orchestrationWorkflowTemplatePath).getLines().mkString("\n")
    val orchestrationWorkflowContent = FileHelper.replaceFromMap(orchestrationWorkflowTemplateContent, executionPlanAttributes)

    FileHelper.writeFile(orchestrationWorkflowsTargetPath, orchestrationWorkflowName + ".XML", orchestrationWorkflowContent)


  })

  println("Done!")

  // -------------------------------------------------------------------------------------------------------------------
  // helper functions

/*
  // uses wrapper file template to create a wrapper file for an Execution Plan
  def writeWrapperFile(execPlanAttribute: Map[String,String]): Unit = {
    val wrapperPageTemplate: String = io.Source.fromFile(wrapperHtmlPath).getLines().mkString("\n")
    val execPlanName = execPlanAttribute("[executionPlanName]")
    val wrapperFileName = "Page__" + execPlanName + ".html"
    val wrapperFileContent = FileHelper.replaceFromMap(wrapperPageTemplate, execPlanAttribute)

    FileHelper.writeFile(svgFileRoot, wrapperFileName, wrapperFileContent)
  }
*/


  // generate DOT string from Execution Plan content
  def getExecutionPlanDot(executionPlanName: String, command: String, executionPlanContent: List[Map[String,String]]): String = {

    val tasksByDepth: Map[String, List[Map[String, String]]] = executionPlanContent groupBy (_("TASK_DEPTH"))
    val depths: List[Int] = tasksByDepth.keys.toList map (_.toInt) sorted
    val depthsInDot: String = (depths mkString(" -> ")) + "[arrowhead=none]"

    val tasksGroupedByDepthInDot: String = (depths
      map (depth => tasksByDepth(depth.toString))
      map (tasksAtCurrentDepth => tasksAtCurrentDepth
          filter (task => task(command) != "") // if a task only has a command for Incremental Load, the Full Load command will be empty and needs to be removed
          map (task => s"{rank=same; ${task("TASK_DEPTH")}; ${DotHelper.formatTaskInDot(command,task)};}" )
          mkString("\n\t")
      )
      mkString("\n\n\t")
    )

    // todo: refactor to DotHelper
    val executionPlanInDot: String =
      s"digraph EP_${executionPlanName.replaceAll("""[ -.\(\)\\/]""","_")} {\n" +
      //s"digraph EP_${executionPlanName.replace(" ","_").replace("-","_").replace(".","_")} {\n" +
      "\tgraph [nodesep=0.1, ranksep=0.25]\n" +
      "\tnode [shape=none, fontname=\"arial\"; fontsize=11]\n" +
      s"\t${depthsInDot}\n\n" +
      "\tnode [shape=box; style=filled; color=\"#BCDC7C\"; fontname=\"arial\"; fontsize=11]\n\n" +
      s"\t$tasksGroupedByDepthInDot\n" +
      "}"

    executionPlanInDot
  }

}
