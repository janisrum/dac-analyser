package EtlAnalysis

import java.io.File
import InformaticaDependencies.InformaticaXmlReader
import scala.xml.Elem
import Common.{DotHelper, FileHelper}

/**
  * Created by Janis Rumnieks on 02/08/2016.
  */

case class ExecutionPlanTask(executionPlanName: String, taskDepth: Int, folderName: String, taskName: String, execType: String, incrementalCommand: String, fullCommand: String, truncFullLoadFlag: String, truncFlag: String)
case class DacContent(allDacTasks: List[ExecutionPlanTask], usedExecutionPlanNames: List[String])
case class SessionWithDepth(sessionName: String, depth: Int, sessionCommand:String, sessionType: String)
case class LinkWithDepth(link: WorkflowLink, depth: Int)
case class ExecutionPlanSessionsAndLinks(sessions: List[SessionWithDepth], internalLinks: List[WorkflowLink], linksFromAccumulator: List[WorkflowLink], linksToAccumulator: List[WorkflowLink], linksForEmptyDepths: List[WorkflowLink])

object OrchestrationGenerator extends App {

  val dacContentFilePath = """C:\Developer\Source Data\Dac Analysis\DAC_content.csv"""
  val scheduledDacContentFilePath = """C:\Developer\Source Data\Dac Analysis\DAC_Execution_Plans.txt"""
  val orchestrationWorkflowsTargetPath = """C:\Developer\Source Data\OrchestrationContent"""
  val orchestrationWorkflowTemplatePath = """C:\Developer\Source Data\OrchestrationContent\Templates\Orchestration__TEMPLATE_4.XML"""
  val infaWorkflowExportsRoot = """C:\Developer\Source Data\InformaticaExports_Workflows"""

  // 1. get DAC Execution Plan content
  val dacContent = getDacExecutionPlanContent

  // 2. Informatica Workflows Sessions and Links
  val informaticaWorkflowContent = getInformaticaWorkflowData

  // 3. iterate through Execution Plans, create an orchestration workflow
  dacContent.usedExecutionPlanNames foreach (executionPlanName => {
    println(s"Generating Orchestration Workflow for DAC Execution Plan $executionPlanName...")

    val execPlanTasks: List[ExecutionPlanTask] = dacContent.allDacTasks filter (_.executionPlanName == executionPlanName)
    val depths = execPlanTasks.map(_.taskDepth).distinct.sorted

    val incrExecPlanTasks = execPlanTasks /*filter (_.execType == "Informatica")*/ filter (_.incrementalCommand != "")
    val fullExecPlanTasks = execPlanTasks /*filter (_.execType == "Informatica")*/ filter (_.fullCommand != "")

    val incrSessionsAndLinks = getExecutionPlanSessionsAndLinksForWorkflows(incrExecPlanTasks, depths, incremental = true)
    val fullSessionsAndLinks = getExecutionPlanSessionsAndLinksForWorkflows(fullExecPlanTasks, depths, incremental = false)

    val incrOrchestrationWorkflowName = "Orchestration__INCR_" + executionPlanName
    val fullOrchestrationWorkflowName = "Orchestration__FULL_" + executionPlanName

    val incrShellScriptTasks = (incrSessionsAndLinks.sessions
      filter (_.sessionType == "External Program")
      map (session =>
      s"""        <TASK DESCRIPTION ="" NAME ="${session.sessionName}" REUSABLE ="NO" TYPE ="Command" VERSIONNUMBER ="1">
          |            <ATTRIBUTE NAME ="Fail task if any command fails" VALUE ="YES"/>
          |            <ATTRIBUTE NAME ="Recovery Strategy" VALUE ="Fail task and continue workflow"/>
          |            <VALUEPAIR NAME ="Command1" REVERSEASSIGNMENT ="NO" VALUE ="${xml.Utility.escape(session.sessionCommand)}"/>
          |        </TASK>\n""".stripMargin)
      mkString("\n")
      )

    val fullShellScriptTasks = (fullSessionsAndLinks.sessions
      filter (_.sessionType == "External Program")
      map (session =>
      s"""        <TASK DESCRIPTION ="" NAME ="${session.sessionName}" REUSABLE ="NO" TYPE ="Command" VERSIONNUMBER ="1">
          |            <ATTRIBUTE NAME ="Fail task if any command fails" VALUE ="YES"/>
          |            <ATTRIBUTE NAME ="Recovery Strategy" VALUE ="Fail task and continue workflow"/>
          |            <VALUEPAIR NAME ="Command1" REVERSEASSIGNMENT ="NO" VALUE ="${xml.Utility.escape(session.sessionCommand)}"/>
          |        </TASK>\n""".stripMargin)
      mkString("\n")
      )

    val incrStoredProcedureTasks = (incrSessionsAndLinks.sessions
      filter (_.sessionType == "Stored Procedure")
      map (session =>
        s"""        <SESSION DESCRIPTION ="" ISVALID ="YES" MAPPINGNAME ="Call_Stored_Procedure" NAME ="${session.sessionName}" REUSABLE ="NO" SORTORDER ="Binary" VERSIONNUMBER ="1">
           |            <SESSTRANSFORMATIONINST ISREPARTITIONPOINT ="YES" PARTITIONTYPE ="PASS THROUGH" PIPELINE ="1" SINSTANCENAME ="DUMMY1" STAGE ="1" TRANSFORMATIONNAME ="DUMMY1" TRANSFORMATIONTYPE ="Target Definition"/>
           |            <SESSTRANSFORMATIONINST ISREPARTITIONPOINT ="NO" PIPELINE ="0" SINSTANCENAME ="DUMMY" STAGE ="0" TRANSFORMATIONNAME ="DUMMY" TRANSFORMATIONTYPE ="Source Definition"/>
           |            <SESSTRANSFORMATIONINST ISREPARTITIONPOINT ="YES" PARTITIONTYPE ="PASS THROUGH" PIPELINE ="1" SINSTANCENAME ="SQ_DUMMY" STAGE ="2" TRANSFORMATIONNAME ="SQ_DUMMY" TRANSFORMATIONTYPE ="Source Qualifier">
           |                <ATTRIBUTE NAME ="Post SQL" VALUE ="call ${session.sessionCommand}();"/>
           |            </SESSTRANSFORMATIONINST>
           |            <CONFIGREFERENCE REFOBJECTNAME ="default_session_config" TYPE ="Session config">
           |                <ATTRIBUTE NAME ="Stop on errors" VALUE ="1"/>
           |            </CONFIGREFERENCE>
           |            <SESSIONEXTENSION NAME ="Relational Writer" SINSTANCENAME ="DUMMY1" SUBTYPE ="Relational Writer" TRANSFORMATIONTYPE ="Target Definition" TYPE ="WRITER">
           |                <CONNECTIONREFERENCE CNXREFNAME ="DB Connection" CONNECTIONNAME ="" CONNECTIONNUMBER ="1" CONNECTIONSUBTYPE ="" CONNECTIONTYPE ="Relational" VARIABLE ="$$DBConnection_OLAP"/>
           |                <ATTRIBUTE NAME ="Target load type" VALUE ="Bulk"/>
           |                <ATTRIBUTE NAME ="Insert" VALUE ="YES"/>
           |                <ATTRIBUTE NAME ="Update as Update" VALUE ="YES"/>
           |                <ATTRIBUTE NAME ="Update as Insert" VALUE ="NO"/>
           |                <ATTRIBUTE NAME ="Update else Insert" VALUE ="NO"/>
           |                <ATTRIBUTE NAME ="Delete" VALUE ="YES"/>
           |                <ATTRIBUTE NAME ="Truncate target table option" VALUE ="NO"/>
           |                <ATTRIBUTE NAME ="Reject file directory" VALUE ="$$PMBadFileDir&#x5c;"/>
           |                <ATTRIBUTE NAME ="Reject filename" VALUE ="dummy11.bad"/>
           |            </SESSIONEXTENSION>
           |            <SESSIONEXTENSION DSQINSTNAME ="SQ_DUMMY" DSQINSTTYPE ="Source Qualifier" NAME ="Relational Reader" SINSTANCENAME ="DUMMY" SUBTYPE ="Relational Reader" TRANSFORMATIONTYPE ="Source Definition" TYPE ="READER"/>
           |            <SESSIONEXTENSION NAME ="Relational Reader" SINSTANCENAME ="SQ_DUMMY" SUBTYPE ="Relational Reader" TRANSFORMATIONTYPE ="Source Qualifier" TYPE ="READER">
           |                <CONNECTIONREFERENCE CNXREFNAME ="DB Connection" CONNECTIONNAME ="" CONNECTIONNUMBER ="1" CONNECTIONSUBTYPE ="" CONNECTIONTYPE ="Relational" VARIABLE ="$$DBConnection_OLAP"/>
           |            </SESSIONEXTENSION>
           |            <ATTRIBUTE NAME ="General Options" VALUE =""/>
           |            <ATTRIBUTE NAME ="Write Backward Compatible Session Log File" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Session Log File Name" VALUE ="s_Call_Stored_Procedure.log"/>
           |            <ATTRIBUTE NAME ="Session Log File directory" VALUE ="$$PMSessionLogDir&#x5c;"/>
           |            <ATTRIBUTE NAME ="Parameter Filename" VALUE =""/>
           |            <ATTRIBUTE NAME ="Enable Test Load" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="$$Source connection value" VALUE =""/>
           |            <ATTRIBUTE NAME ="$$Target connection value" VALUE =""/>
           |            <ATTRIBUTE NAME ="Treat source rows as" VALUE ="Insert"/>
           |            <ATTRIBUTE NAME ="Commit Type" VALUE ="Target"/>
           |            <ATTRIBUTE NAME ="Commit Interval" VALUE ="10000"/>
           |            <ATTRIBUTE NAME ="Commit On End Of File" VALUE ="YES"/>
           |            <ATTRIBUTE NAME ="Rollback Transactions on Errors" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Recovery Strategy" VALUE ="Fail task and continue workflow"/>
           |            <ATTRIBUTE NAME ="Java Classpath" VALUE =""/>
           |            <ATTRIBUTE NAME ="Performance" VALUE =""/>
           |            <ATTRIBUTE NAME ="DTM buffer size" VALUE ="Auto"/>
           |            <ATTRIBUTE NAME ="Collect performance data" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Write performance data to repository" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Incremental Aggregation" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Enable high precision" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Session retry on deadlock" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Pushdown Optimization" VALUE ="None"/>
           |            <ATTRIBUTE NAME ="Allow Temporary View for Pushdown" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Allow Temporary Sequence for Pushdown" VALUE ="NO"/>
           |            <ATTRIBUTE NAME ="Allow Pushdown for User Incompatible Connections" VALUE ="NO"/>
           |        </SESSION>
         """.stripMargin)
      mkString("\n")
      )


    val fullStoredProcedureTasks = (fullSessionsAndLinks.sessions
      filter (_.sessionType == "Stored Procedure")
      map (session =>
      s"""        <SESSION DESCRIPTION ="" ISVALID ="YES" MAPPINGNAME ="Call_Stored_Procedure" NAME ="${session.sessionName}" REUSABLE ="NO" SORTORDER ="Binary" VERSIONNUMBER ="1">
          |            <SESSTRANSFORMATIONINST ISREPARTITIONPOINT ="YES" PARTITIONTYPE ="PASS THROUGH" PIPELINE ="1" SINSTANCENAME ="DUMMY1" STAGE ="1" TRANSFORMATIONNAME ="DUMMY1" TRANSFORMATIONTYPE ="Target Definition"/>
          |            <SESSTRANSFORMATIONINST ISREPARTITIONPOINT ="NO" PIPELINE ="0" SINSTANCENAME ="DUMMY" STAGE ="0" TRANSFORMATIONNAME ="DUMMY" TRANSFORMATIONTYPE ="Source Definition"/>
          |            <SESSTRANSFORMATIONINST ISREPARTITIONPOINT ="YES" PARTITIONTYPE ="PASS THROUGH" PIPELINE ="1" SINSTANCENAME ="SQ_DUMMY" STAGE ="2" TRANSFORMATIONNAME ="SQ_DUMMY" TRANSFORMATIONTYPE ="Source Qualifier">
          |                <ATTRIBUTE NAME ="Post SQL" VALUE ="call ${session.sessionCommand}();"/>
          |            </SESSTRANSFORMATIONINST>
          |            <CONFIGREFERENCE REFOBJECTNAME ="default_session_config" TYPE ="Session config">
          |                <ATTRIBUTE NAME ="Stop on errors" VALUE ="1"/>
          |            </CONFIGREFERENCE>
          |            <SESSIONEXTENSION NAME ="Relational Writer" SINSTANCENAME ="DUMMY1" SUBTYPE ="Relational Writer" TRANSFORMATIONTYPE ="Target Definition" TYPE ="WRITER">
          |                <CONNECTIONREFERENCE CNXREFNAME ="DB Connection" CONNECTIONNAME ="" CONNECTIONNUMBER ="1" CONNECTIONSUBTYPE ="" CONNECTIONTYPE ="Relational" VARIABLE ="$$DBConnection_OLAP"/>
          |                <ATTRIBUTE NAME ="Target load type" VALUE ="Bulk"/>
          |                <ATTRIBUTE NAME ="Insert" VALUE ="YES"/>
          |                <ATTRIBUTE NAME ="Update as Update" VALUE ="YES"/>
          |                <ATTRIBUTE NAME ="Update as Insert" VALUE ="NO"/>
          |                <ATTRIBUTE NAME ="Update else Insert" VALUE ="NO"/>
          |                <ATTRIBUTE NAME ="Delete" VALUE ="YES"/>
          |                <ATTRIBUTE NAME ="Truncate target table option" VALUE ="NO"/>
          |                <ATTRIBUTE NAME ="Reject file directory" VALUE ="$$PMBadFileDir&#x5c;"/>
          |                <ATTRIBUTE NAME ="Reject filename" VALUE ="dummy11.bad"/>
          |            </SESSIONEXTENSION>
          |            <SESSIONEXTENSION DSQINSTNAME ="SQ_DUMMY" DSQINSTTYPE ="Source Qualifier" NAME ="Relational Reader" SINSTANCENAME ="DUMMY" SUBTYPE ="Relational Reader" TRANSFORMATIONTYPE ="Source Definition" TYPE ="READER"/>
          |            <SESSIONEXTENSION NAME ="Relational Reader" SINSTANCENAME ="SQ_DUMMY" SUBTYPE ="Relational Reader" TRANSFORMATIONTYPE ="Source Qualifier" TYPE ="READER">
          |                <CONNECTIONREFERENCE CNXREFNAME ="DB Connection" CONNECTIONNAME ="" CONNECTIONNUMBER ="1" CONNECTIONSUBTYPE ="" CONNECTIONTYPE ="Relational" VARIABLE ="$$DBConnection_OLAP"/>
          |            </SESSIONEXTENSION>
          |            <ATTRIBUTE NAME ="General Options" VALUE =""/>
          |            <ATTRIBUTE NAME ="Write Backward Compatible Session Log File" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Session Log File Name" VALUE ="s_Call_Stored_Procedure.log"/>
          |            <ATTRIBUTE NAME ="Session Log File directory" VALUE ="$$PMSessionLogDir&#x5c;"/>
          |            <ATTRIBUTE NAME ="Parameter Filename" VALUE =""/>
          |            <ATTRIBUTE NAME ="Enable Test Load" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="$$Source connection value" VALUE =""/>
          |            <ATTRIBUTE NAME ="$$Target connection value" VALUE =""/>
          |            <ATTRIBUTE NAME ="Treat source rows as" VALUE ="Insert"/>
          |            <ATTRIBUTE NAME ="Commit Type" VALUE ="Target"/>
          |            <ATTRIBUTE NAME ="Commit Interval" VALUE ="10000"/>
          |            <ATTRIBUTE NAME ="Commit On End Of File" VALUE ="YES"/>
          |            <ATTRIBUTE NAME ="Rollback Transactions on Errors" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Recovery Strategy" VALUE ="Fail task and continue workflow"/>
          |            <ATTRIBUTE NAME ="Java Classpath" VALUE =""/>
          |            <ATTRIBUTE NAME ="Performance" VALUE =""/>
          |            <ATTRIBUTE NAME ="DTM buffer size" VALUE ="Auto"/>
          |            <ATTRIBUTE NAME ="Collect performance data" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Write performance data to repository" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Incremental Aggregation" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Enable high precision" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Session retry on deadlock" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Pushdown Optimization" VALUE ="None"/>
          |            <ATTRIBUTE NAME ="Allow Temporary View for Pushdown" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Allow Temporary Sequence for Pushdown" VALUE ="NO"/>
          |            <ATTRIBUTE NAME ="Allow Pushdown for User Incompatible Connections" VALUE ="NO"/>
          |        </SESSION>
         """.stripMargin)
      mkString("\n")
      )



    val incrSessionTaskInstances = incrSessionsAndLinks.sessions /*filter (_.sessionType == "Informatica")*/ map (session => s"""        <TASKINSTANCE DESCRIPTION ="" FAIL_PARENT_IF_INSTANCE_DID_NOT_RUN ="YES" FAIL_PARENT_IF_INSTANCE_FAILS ="YES" ISENABLED ="YES" NAME ="${session.sessionName}" REUSABLE ="${if (session.sessionType == "Informatica") "YES" else "NO"}" TASKNAME ="${session.sessionName}" TASKTYPE ="${if (session.sessionType == "External Program") "Command" else "Session"}" TREAT_INPUTLINK_AS_AND ="YES"/>""") mkString("\n")
    val fullSessionTaskInstances = fullSessionsAndLinks.sessions /*filter (_.sessionType == "Informatica")*/ map (session => s"""        <TASKINSTANCE DESCRIPTION ="" FAIL_PARENT_IF_INSTANCE_DID_NOT_RUN ="YES" FAIL_PARENT_IF_INSTANCE_FAILS ="YES" ISENABLED ="YES" NAME ="${session.sessionName}" REUSABLE ="${if (session.sessionType == "Informatica") "YES" else "NO"}" TASKNAME ="${session.sessionName}" TASKTYPE ="${if (session.sessionType == "External Program") "Command" else "Session"}" TREAT_INPUTLINK_AS_AND ="YES"/>""") mkString("\n")

    // todo: add criteria for success - Decision Name attribute value
    val decisionTasks = depths map (depth => s"""        <TASK DESCRIPTION ="" NAME ="Depth_$depth" REUSABLE ="NO" TYPE ="Decision" VERSIONNUMBER ="1"><ATTRIBUTE NAME ="Decision Name" VALUE =""/></TASK>""") mkString("\n")

    val decisionTaskInstances = depths map (depth => s"""        <TASKINSTANCE DESCRIPTION ="" FAIL_PARENT_IF_INSTANCE_DID_NOT_RUN ="YES" FAIL_PARENT_IF_INSTANCE_FAILS ="YES" ISENABLED ="YES" NAME ="Depth_$depth" REUSABLE ="NO" TASKNAME ="Depth_$depth" TASKTYPE ="Decision" TREAT_INPUTLINK_AS_AND ="YES"/>""") mkString("\n")

    val incrWorkflowLinksSourceInternal = incrSessionsAndLinks.internalLinks map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")
    val fullWorkflowLinksSourceInternal = fullSessionsAndLinks.internalLinks map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")

    val incrOrchestrationLinksFromAccumulator = incrSessionsAndLinks.linksFromAccumulator map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")
    val incrOrchestrationLinksToAccumulator = incrSessionsAndLinks.linksToAccumulator map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")
    val incrOrchestrationLinksForEmptyDepths = incrSessionsAndLinks.linksForEmptyDepths map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")
    val incrWorkflowLinksOrchestration = "        <!-- Accumulator-to-Task: -->\n" + incrOrchestrationLinksFromAccumulator + "\n\n        <!-- Task-to-Accumulator: -->\n" + incrOrchestrationLinksToAccumulator + "\n\n        <!-- Connecting empty depths (if any): -->\n" + incrOrchestrationLinksForEmptyDepths

    val fullOrchestrationLinksFromAccumulator = fullSessionsAndLinks.linksFromAccumulator map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")
    val fullOrchestrationLinksToAccumulator = fullSessionsAndLinks.linksToAccumulator map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")
    val fullOrchestrationLinksForEmptyDepths = fullSessionsAndLinks.linksForEmptyDepths map (link => s"""        <WORKFLOWLINK CONDITION ="" FROMTASK ="${link.fromTask}" TOTASK ="${link.toTask}"/>""") mkString("\n")
    val fullWorkflowLinksOrchestration = "        <!-- Accumulator-to-Task: -->\n" + fullOrchestrationLinksFromAccumulator + "\n\n        <!-- Task-to-Accumulator: -->\n" + fullOrchestrationLinksToAccumulator + "\n\n        <!-- Connecting empty depths (if any): -->\n" + fullOrchestrationLinksForEmptyDepths

    val incrWorkflowLogFileName = incrOrchestrationWorkflowName + ".log"
    val fullWorkflowLogFileName = fullOrchestrationWorkflowName + ".log"

    val incrExecutionPlanAttributes = Map(
      "[orchestrationWorkflowName]" -> incrOrchestrationWorkflowName,
      "[shellScriptTasks]" -> incrShellScriptTasks,
      "[storedProcedureTasks]" -> incrStoredProcedureTasks,
      "[sessionTaskInstances]" -> incrSessionTaskInstances,
      "[decisionTasks]" -> decisionTasks,
      "[decisionTaskInstances]" -> decisionTaskInstances,
      "[workflowLinksSourceInternal]" -> incrWorkflowLinksSourceInternal,
      "[workflowLinksOrchestration]" -> incrWorkflowLinksOrchestration,
      "[workflowLogFileName]" -> incrWorkflowLogFileName
    )

    val fullExecutionPlanAttributes = Map(
      "[orchestrationWorkflowName]" -> fullOrchestrationWorkflowName,
      "[shellScriptTasks]" -> fullShellScriptTasks,
      "[storedProcedureTasks]" -> fullStoredProcedureTasks,
      "[sessionTaskInstances]" -> fullSessionTaskInstances,
      "[decisionTasks]" -> decisionTasks,
      "[decisionTaskInstances]" -> decisionTaskInstances,
      "[workflowLinksSourceInternal]" -> fullWorkflowLinksSourceInternal,
      "[workflowLinksOrchestration]" -> fullWorkflowLinksOrchestration,
      "[workflowLogFileName]" -> fullWorkflowLogFileName
    )

    val orchestrationWorkflowTemplateContent = scala.io.Source.fromFile(orchestrationWorkflowTemplatePath).getLines().mkString("\n")

    val incrOrchestrationWorkflowContent = FileHelper.replaceFromMap(orchestrationWorkflowTemplateContent, incrExecutionPlanAttributes)
    val fullOrchestrationWorkflowContent = FileHelper.replaceFromMap(orchestrationWorkflowTemplateContent, fullExecutionPlanAttributes)

    FileHelper.writeFile(orchestrationWorkflowsTargetPath, incrOrchestrationWorkflowName + ".XML", incrOrchestrationWorkflowContent)
    FileHelper.writeFile(orchestrationWorkflowsTargetPath, fullOrchestrationWorkflowName + ".XML", fullOrchestrationWorkflowContent)

  })

  println("Done!")

  // -------------------------------------------------------------------------------------------------------------------
  // helper functions

  def getDacExecutionPlanContent = {
    println(s"Getting DAC Execution Plan content from $dacContentFilePath csv file...")

    val allDacContent: CsvContent = FileHelper.readCsvFile(dacContentFilePath)
    val allDacTasks = allDacContent.content map
      (row => ExecutionPlanTask(
        executionPlanName = row("EXEC_PLAN_NAME"),
        taskDepth = row("TASK_DEPTH").toInt,
        folderName = row("FOLDER_NAME"),
        taskName = row("TASK_NAME"),
        execType = row("EXEC_TYPE"),
        incrementalCommand = row("INCREMENTAL_COMMAND"),
        fullCommand = row("FULL_COMMAND"),
        truncFullLoadFlag = row("TRUNC_FULLLOAD_FLG"),
        truncFlag = row("TRUNC_FLG")
      )
        )

    val allDacExecutionPlanNames = (allDacTasks map (_.executionPlanName) distinct) sorted

    val usedExecutionPlanNamesScheduled: Set[String] = FileHelper.readTxtFile(scheduledDacContentFilePath)
    val usedExecutionPlanNames = allDacExecutionPlanNames filter (usedExecutionPlanNamesScheduled contains (_))

    DacContent(allDacTasks = allDacTasks, usedExecutionPlanNames = usedExecutionPlanNames)
  }


  def getInformaticaWorkflowData = {
    println(s"Getting Informatica Execution Plan data from the XML files in folder $infaWorkflowExportsRoot...")

    val infaWorkflowSourceFiles: List[File] = InformaticaXmlReader.filesFromFolder(infaWorkflowExportsRoot)

    val workflowSessionsAndLinks: List[WorkflowSessionsAndLinks] = infaWorkflowSourceFiles flatMap (
      file => {
        //println(s"Interpreting Workflow XML from ${file.getPath}...")
        val xmlContent: Elem = InformaticaXmlReader.xmlFileContent(file)
        InformaticaXmlReader.workflowSessionsAndLinksFromXml(xmlContent)
      }
      )

    workflowSessionsAndLinks
  }


  def getExecutionPlanSessionsAndLinksForWorkflows(dacTasks: List[ExecutionPlanTask], depths: List[Int], incremental: Boolean) = {


    // Informatica Sessions and Internal Links
    val (incrementalSessionsNotFlat, incrementalWorkflowLinksNotFlat) = (for {
      workflowTask <- dacTasks if workflowTask.execType == "Informatica"
      workflowTaskCommand = if (incremental) workflowTask.incrementalCommand else workflowTask.fullCommand
      workflowContent <- informaticaWorkflowContent if workflowContent.workflowName == workflowTaskCommand
      sessions = workflowContent.sessions map (session => SessionWithDepth(sessionName = session, depth = workflowTask.taskDepth, sessionCommand = session, sessionType = "Informatica"))
      links = workflowContent.links map (link => LinkWithDepth(link = link, depth = workflowTask.taskDepth))
    } yield (sessions, links)).unzip

    val workflowSessions = incrementalSessionsNotFlat.flatten
    val workflowInternalLinks = incrementalWorkflowLinksNotFlat.flatten filter (workflowLink => workflowLink.link.fromTask != "Start")

    val workflowSessionEntryPoints = workflowSessions filter (session => !(workflowInternalLinks.exists(internalLink => internalLink.link.toTask == session.sessionName)))
    val workflowSessionExitPoints = workflowSessions filter (session => !(workflowInternalLinks.exists(internalLink => internalLink.link.fromTask == session.sessionName)))


    // Stored Procedures and Shell scripts
    val storedProcedureSessions = for {
      storedProcedureTask <- dacTasks if storedProcedureTask.execType == "Stored Procedure"
      storedProcedureTaskCommand = if (incremental) storedProcedureTask.incrementalCommand else storedProcedureTask.fullCommand
      session = SessionWithDepth(sessionName = "StoredProcedure__" + storedProcedureTask.taskName, depth = storedProcedureTask.taskDepth, sessionCommand = storedProcedureTaskCommand, sessionType = "Stored Procedure")
    } yield(session)

    val shellScriptSessions = for {
      shellScriptTask <- dacTasks if shellScriptTask.execType == "External Program"
      shellScriptTaskCommand = if (incremental) shellScriptTask.incrementalCommand else shellScriptTask.fullCommand
      session = SessionWithDepth(sessionName = "ShellScript__" + shellScriptTask.taskName, depth = shellScriptTask.taskDepth, sessionCommand = shellScriptTaskCommand, sessionType = "External Program")
    } yield(session)


    // Building Orchestration workflow content
    val startDepth = depths.head

    val orchestrationLinksFromAccumulator = for {
      depth <- depths
      fromAccumulator = if(depth == startDepth) "Start" else s"Depth_${depth-1}"
      allEntryPoints = workflowSessionEntryPoints ++ storedProcedureSessions ++ shellScriptSessions
      entryPointSession <- allEntryPoints if entryPointSession.depth == depth
      toTask = entryPointSession.sessionName
    } yield WorkflowLink(fromTask = fromAccumulator, toTask = toTask) //(s"""\t\t<WORKFLOWLINK CONDITION ="" FROMTASK ="$fromAccumulator" TOTASK ="$toTask"/>""")

    val orchestrationLinksToAccumulator = for {
      depth <- depths
      toAccumulator = s"Depth_${depth}"
      allExitPoints = workflowSessionExitPoints ++ storedProcedureSessions ++ shellScriptSessions
      exitPointSession <- allExitPoints if exitPointSession.depth == depth
      fromTask = exitPointSession.sessionName
    } yield WorkflowLink(fromTask = fromTask, toTask = toAccumulator) //(s"""\t\t<WORKFLOWLINK CONDITION ="" FROMTASK ="$fromTask" TOTASK ="$toAccumulator"/>""")

    val allSessions = workflowSessions ++ storedProcedureSessions ++ shellScriptSessions

    val linksForEmptyDepthsOptions = for {
      depth <- depths
      tasksExistAtDepth = allSessions exists(_.depth == depth)
      linkForEmptyDepth = if (tasksExistAtDepth) None else {
          val fromAccumulator = if(depth == startDepth) "Start" else s"Depth_${depth-1}"
          val toAccumulator = s"Depth_${depth}"
          Some( WorkflowLink(fromTask = fromAccumulator, toTask = toAccumulator) )
        }
    } yield linkForEmptyDepth

    val linksForEmptyDepths = linksForEmptyDepthsOptions filter (_ != None) map (_.get)


    ExecutionPlanSessionsAndLinks(sessions = allSessions, internalLinks = workflowInternalLinks map (_.link), linksFromAccumulator = orchestrationLinksFromAccumulator, linksToAccumulator = orchestrationLinksToAccumulator, linksForEmptyDepths = linksForEmptyDepths)
  }



}
