import java.io.File

import scala.io.Source
import scala.util.matching.Regex
import scala.xml.{Elem, XML}

/**
  * Created by Janis Rumnieks on 29/07/2016.
  */
object XmlHelper_BACKUP {

  private def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }

  case class XmlFile(fileName: String, fileContent: String)

  private def removeWeirdTags(f: File): XmlFile = {
    val content: Iterator[String] = Source.fromFile(f).getLines()

    val invalidXml: Regex = "[^\u0009\u000A\u000D\u0020-\uD7FF\uE000-\uFFFD\u10000-\u10FFF]+".r
    val xml11pattern: Regex = ( "[^"
                              + "\u0001-\uD7FF"
                              + "\uE000-\uFFFD"
                              + "\ud800\udc00-\udbff\udfff"
                              + "]+").r

    val filteredContent = (
      content
        filter (c => if (c == """<!DOCTYPE POWERMART SYSTEM "powrmart.dtd">""") false else true)
        map( c => invalidXml.replaceAllIn(c, "") )
        mkString("\n")
      )
    XmlFile(f.getPath, filteredContent)
  }


  def getXmlsFromFolder(folderName: String): List[Elem] = (
    getListOfFiles(folderName)
      //map( f => { println(f.getPath); f } )
      map(removeWeirdTags)
      map( x => { println(x.fileName); XML.loadString(x.fileContent) } )
    )

}
