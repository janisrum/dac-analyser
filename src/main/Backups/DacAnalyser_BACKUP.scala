import InformaticaDependencies.{WorkflowMappings, FolderContent, InformaticaXmlReader}

import java.io.{File, PrintWriter}
import scala.io.Source
import scala.reflect._
import scala.xml.Elem

/**
  * Created by Janis Rumnieks on 02/08/2016.
  */

case class CsvContent(fileName: String, header: List[String], content: List[Map[String,String]])

object DacAnalyser extends App {

  // ---------------------------------------------------
  // Summary of all DAC content
  // draw DOTs todo: refactor this
  val dacContentFilePath = """C:\Developer\Source Data\Dac Analysis\DAC_content.csv"""
  val dotFileRoot = """C:\Developer\Source Data\Dac Analysis\Dac Execution Plans"""

  val allDacContent = readCsvFile(dacContentFilePath)
  val allDacContentByExecPlan = allDacContent.content groupBy(_("EXEC_PLAN_NAME"))
  val allDacExecPlans = allDacContentByExecPlan.keys

  allDacExecPlans foreach (ep => {
    println(s"Processing $ep...")

    val fileNameIncremental = "INCR__" + ep + ".dot"
    val dotContentIncremental = getExecutionPlanDot(ep, "INCREMENTAL_COMMAND", allDacContentByExecPlan(ep))
    writeFile(dotFileRoot, fileNameIncremental, dotContentIncremental)

    val fileNameFull = "FULL__" + ep + ".dot"
    val dotContentFull = getExecutionPlanDot(ep, "FULL_COMMAND", allDacContentByExecPlan(ep))
    writeFile(dotFileRoot, fileNameFull, dotContentFull)
  })

  case class ExecPlanContentSummary(executionPlanName: String, numberOfTasks: Integer, numberOfTasksInformatica: Integer, numberOfTasksStoredProcedure: Integer, numberOfTasksScript: Integer)
  val allExecPlanTasksSummary: Iterable[ExecPlanContentSummary] = allDacExecPlans map (
    ep =>
      ExecPlanContentSummary(
        executionPlanName = ep,
        numberOfTasks = allDacContentByExecPlan(ep).length,
        numberOfTasksInformatica = allDacContentByExecPlan(ep).filter(_("EXEC_TYPE") == "Informatica").length,
        numberOfTasksStoredProcedure = allDacContentByExecPlan(ep).filter(_("EXEC_TYPE") == "Stored Procedure").length,
        numberOfTasksScript = allDacContentByExecPlan(ep).filter(_("EXEC_TYPE") == "External Program").length
      )
    )

  writeCsvFile("""C:\Developer\Source Data\Dac Analysis""", "all_execution_plan_tasks_summary.csv", allExecPlanTasksSummary.toList)

  // ---------------------------------------------------
  // Summary of used DAC content
  val usedExecutionPlans = readTxtFile("""C:\Developer\Source Data\Dac Analysis\DAC_Execution_Plans.txt""")

  val usedExecutionPlanTasksSummary: List[ExecPlanContentSummary] = for {
    executionPlan <- usedExecutionPlans.toList.sorted
    planTask <- allExecPlanTasksSummary if planTask.executionPlanName == executionPlan
  } yield planTask

  //val csvContentUsedPlans = caseClassToCsvString(usedExecutionPlanTasksSummary)
  //writeFile("""C:\Developer\Source Data\Dac Analysis""", "used_execution_plan_tasks_summary.csv", csvContentUsedPlans) // used_plan_tasks.csv

  writeCsvFile("""C:\Developer\Source Data\Dac Analysis""", "used_execution_plan_tasks_summary.csv", usedExecutionPlanTasksSummary) // used_plan_tasks.csv

  // ---------------------------------------------------
  // used Informatica Workflows and Mappings
  case class InfaWorkflow(folderName: String, workflowName: String)
  case class InfaMapping(folderName: String, mappingName: String)

  val usedInformaticaWorkflows: Set[InfaWorkflow] = (
    allDacContent.content
      filter (usedExecutionPlans contains _("EXEC_PLAN_NAME"))
      filter (_("EXEC_TYPE") == "Informatica")
      flatMap (c => List(
          InfaWorkflow(c("FOLDER_NAME"), c("INCREMENTAL_COMMAND")),
          InfaWorkflow(c("FOLDER_NAME"), c("FULL_COMMAND"))
        ) filter (_.workflowName != "") )
      toSet
    ) // 283

  val usedInformaticaWorkflowsOrdered = usedInformaticaWorkflows.toList sortWith ((w1, w2) => (w1.folderName < w2.folderName || w1.folderName == w2.folderName && w1.workflowName < w2.workflowName) )

  writeCsvFile("""C:\Developer\Source Data\Dac Analysis""", "used_informatica_workflows.csv", usedInformaticaWorkflowsOrdered)

  // workflows to mappings
  val infaWorkflowExportsRoot = """C:\Developer\Source Data\InformaticaExports_Workflows"""
  val infaWorkflowSourceFiles: List[File] = InformaticaXmlReader.filesFromFolder(infaWorkflowExportsRoot)

  val informaticaWorkflowsToMappings: List[WorkflowMappings] = infaWorkflowSourceFiles flatMap (
    file => {
      println(s"Interpreting Workflow XML from ${file.getPath}...")
      val xmlContent: Elem = InformaticaXmlReader.xmlFileContent(file)
      InformaticaXmlReader.workflowsFromXml(xmlContent)
    }
    )

  val usedInformaticaMappings = usedInformaticaWorkflows flatMap (w => (informaticaWorkflowsToMappings filter (w2m => w2m.workflowName == w.workflowName) head).mappings map (m => InfaMapping(w.folderName, m)))
  val usedInformaticaMappingsOrdered = usedInformaticaMappings.toList sortWith ((m1, m2) => (m1.folderName < m2.folderName || m1.folderName == m2.folderName && m1.mappingName < m2.mappingName) )

  writeCsvFile("""C:\Developer\Source Data\Dac Analysis""", "used_informatica_mappings.csv", usedInformaticaMappingsOrdered)


  // ---------------------------------------------------
  // find DAC folders for the BI Apps Informatica mappings
  // BI Apps only!
  case class BiAppsMappingInfo(mappingName: String, usedExecutionPlansThatInclude: String, allExecutionPlansThatInclude: String)

  val usedBiAppsMappings = readTxtFile("""C:\Developer\Source Data\Dac Analysis\BI Apps Mappings User by Hays.txt""").toList.sorted

  val biAppsMappingsAndTheirExecutionPlans = for {
    biAppsMapping <- usedBiAppsMappings
    allExecPlansThatInclude = (allDacContent.content filter (c => c("INCREMENTAL_COMMAND") == biAppsMapping || c("FULL_COMMAND") == biAppsMapping) map (c => c("EXEC_PLAN_NAME"))).toSet
    usedExecPlansThatInclude = allExecPlansThatInclude intersect usedExecutionPlans
  } yield( BiAppsMappingInfo(mappingName = biAppsMapping, usedExecutionPlansThatInclude = usedExecPlansThatInclude mkString(","), allExecutionPlansThatInclude = allExecPlansThatInclude mkString(",") ) )

  writeCsvFile("""C:\Developer\Source Data\Dac Analysis""", "BI Apps Mappings and their Execution Plans.csv", biAppsMappingsAndTheirExecutionPlans)


  // ---------------------------------------------------
  // table level analysis - tables that are mapping targets

  // 1. get all Infa mapping info from Infa XMLs
  val infaExportsRoot = """C:\Developer\Source Data\InformaticaExports"""
  val dotsRoot = """C:\Developer\Source Data\DOTs"""
  val infaSourceFiles: List[File] = InformaticaXmlReader.filesFromFolder(infaExportsRoot)

  val allInfaFolderContents: List[FolderContent] = infaSourceFiles map (
    file => {
      println(s"Interpreting Mapping XML from ${file.getPath}...")
      val xmlContent = InformaticaXmlReader.xmlFileContent(file)
      InformaticaXmlReader.mappingDependencies(xmlContent)
    }
    )

  // 2. list of empty target tables (generated based on step 3)
  // empty tables analysis - which mappings relate to the empty tables. which mappings could be excluded from the ETL
  // find all mappings which have only empty tables as targets
  //val emptyTargetTables = readTxtFile("""C:\Developer\Source Data\Dac Analysis\Empty Target Tables.csv""").toList.sorted

  // 3. get used Infa mappings and their targets
  case class mappingTarget(mappingTarget: String, mappingName: String, folderName: String)

  val usedMappingTargets = for {
    infaFolderContent <- allInfaFolderContents
    usedMapping <- infaFolderContent.folderMappings filter (m => usedInformaticaMappings contains (InfaMapping(infaFolderContent.folderName, m.mappingName)) ) // filter only used mappings
    targetTable <- usedMapping.mappingTargets
    //targetIsEmpty = if ((emptyTargetTables filter (_ == targetTable.tableName)) == Nil) false else true
  } yield ( mappingTarget(folderName = infaFolderContent.folderName, mappingName = usedMapping.mappingName, mappingTarget = targetTable.tableName) )

  val biMappingTargetsSorted = usedMappingTargets.sortWith((t1, t2) => if(t1.mappingTarget < t2.mappingTarget || t1.mappingTarget == t2.mappingTarget && t1.mappingName < t2.mappingName) true else false )

  writeCsvFile("""C:\Developer\Source Data\Dac Analysis""", "used_mapping_targets.csv", biMappingTargetsSorted)


  // 4. get distinct target tables
  val usedTargetTables = (usedMappingTargets map (mt => mt.mappingTarget) distinct) mkString("\n")
  writeFile("""C:\Developer\Source Data\Dac Analysis""", "used_target_tables.csv", usedTargetTables)



  //val numOfTargets = (usedMappingTargets map (mt => mt.mappingTarget) toSet).size
  //println(s"Used Targets: $numOfTargets")





  // 4. mappigns with only empty tables as targets
//  case class MappingWithEmptyTargets(mappingName: String, targetsList: String)

//  val mappingsWithEmptyTargets = (
//    biMappingTargets
//      groupBy (mt => mt.mappingName)
//      filter {case(mapping,content) => content forall (c => c.targetIsEmpty)}
//      map {case(mapping,content) => MappingWithEmptyTargets(mapping, content map (_.mappingTarget) mkString ",")}
//    ).toList.sortWith((m1,m2) => if(m1.mappingName < m2.mappingName) true else false)

//  val csvContentMappingsWithEmptyTargets = caseClassToCsvString(mappingsWithEmptyTargets)
//  writeFile("""C:\Developer\Source Data\Dac Analysis""", "Mappings with Empty Targets.csv", csvContentMappingsWithEmptyTargets)



  /*
    val infaExportsRoot = """C:\Developer\Source Data\InformaticaExports"""
    val dotsRoot = """C:\Developer\Source Data\DOTs"""
    val infaSourceFiles: List[File] = InformaticaXmlReader.filesFromFolder(infaExportsRoot)

    val allInfaFolderContents: List[FolderContent] = infaSourceFiles map (
      file => {
        println(s"Interpreting XML from ${file.getPath}...")
        val xmlContent = InformaticaXmlReader.xmlFileContent(file)
        InformaticaXmlReader.mappingDependencies(xmlContent)
      }
      )

    val biAppsUsedInfaFolders = Set("SDE_PSFT_90_Adapter","SILOS","PLP")
    val biAppsUsedFolderContents = allInfaFolderContents filter (fc => biAppsUsedInfaFolders.contains(fc.folderName) )
    //val usedMappings = usedBiAppsMappings.toSet

    // empty tables analysis - which mappings relate to the empty tables. which mappings could be excluded from the ETL
    // find all mappings which have only empty tables as targets
    val emptyTargetTables = readTxtFile("""C:\Developer\Source Data\Dac Analysis\Empty Target Tables.csv""").toList.sorted

    case class mappingTarget(mappingTarget: String, targetIsEmpty: Boolean, mappingName: String, folderName: String) //todo: extends Ordered[mappingTarget]

    val biMappingTargets = for {
      folderContent <- biAppsUsedFolderContents
      mapping <- folderContent.folderMappings //filter (m => usedMappings.contains(m.mappingName)) // filter only allows BI Apps mappings
      targetTable <- mapping.mappingTargets
      targetIsEmpty = if ((emptyTargetTables filter (_ == targetTable.tableName)) == Nil) false else true
    } yield ( mappingTarget(folderName = folderContent.folderName, mappingName = mapping.mappingName, mappingTarget = targetTable.tableName, targetIsEmpty = targetIsEmpty) )
    val biMappingTargetsSorted = biMappingTargets.sortWith( (t1,t2) => if(t1.mappingTarget < t2.mappingTarget || t1.mappingTarget < t2.mappingTarget && t1.mappingName < t2.mappingName) true else false )

    writeCsvFile("""C:\Developer\Source Data\Dac Analysis""", "BI Apps Mapping Targets.csv", biMappingTargetsSorted)

    val numOfTargets = (biMappingTargets map (mt => mt.mappingTarget) toSet).size
    println(s"Used Targets: $numOfTargets")


     */









  val xxxxxxx = 1
  println("Done!")

// ---------------------------------------------------------------------------------------------------------------------
// helper functions
def readTxtFile(filePathAndName: String) =
  Source.fromFile(filePathAndName).getLines.map(_.replace("\t","")).filter(l => (l.trim != "" && l.trim()(0) != '#')).toSet


  def caseClassToCsvString[A](sourceContent: List[A])(implicit c: ClassTag[A]): String = {
    val classFields = classTag[A].runtimeClass.getDeclaredFields
    val classFieldNames = classFields map(_.getName)
    val csvHeader = classFieldNames  mkString(",")
    val csvContentRows = for {
      contentRow <- sourceContent
      rowString = classFields map (field => {field.setAccessible(true); field.get(contentRow)}) map ("\"" + _ + "\"") mkString(",")
    } yield rowString

    csvHeader + "\n" + csvContentRows.mkString("\n")
  }

  def writeFile(root: String, fileName: String, content: String): Unit = {
    val pw = new PrintWriter(new File(s"$root\\$fileName" ))
    pw.write(content)
    pw.close
  }

  def writeCsvFile[A](root: String, fileName: String, sourceContent: List[A])(implicit c: ClassTag[A]): Unit = {
    val content = caseClassToCsvString(sourceContent)
    writeFile(root, fileName, content)
  }


  def getExecutionPlanDot(executionPlanName: String, command: String, executionPlanContent: List[Map[String,String]]): String = {
    val tasksByDepth = executionPlanContent groupBy (_("TASK_DEPTH"))
    val depths = tasksByDepth.keys.toList map (_.toInt) sorted

    def formatTask(task: Map[String,String]): String = {
      "\"" +
      task(command) +
      "\"" +
      (
        if      (task("EXEC_TYPE") == "Stored Procedure") "[color=palevioletred1;]"
        else if (task("EXEC_TYPE") == "External Program") "[color=palegreen;]"
        else ""
      )
      //task("FOLDER_NAME") + task("EXEC_TYPE") +
    }


    val depthsDot = (depths mkString(" -> ")) + "[arrowhead=none]"
    val rankSame = depths map (d => tasksByDepth(d.toString)) map (tasks => tasks filter (task => task("INCREMENTAL_COMMAND") != "") map (task => s"{ rank=same; ${task("TASK_DEPTH")}; ${formatTask(task)};}" ) mkString("\n\t") ) mkString("\n\n\t")

    s"digraph EP_${executionPlanName.replace(" ","_").replace("-","_").replace(".","_")} {\n" +
    "\tgraph [nodesep=0.1, ranksep=0.25]\n" +
    "\tnode [shape=none, fontname=\"arial\"; fontsize=11]\n" +
    s"\t${depthsDot}\n\n" +
    "\tnode [shape=box; style=filled; color=lightblue; fontname=\"arial\"; fontsize=11]\n\n" +
    s"\t$rankSame\n" +
    "}"
  }

  def readCsvFile(fileName: String): CsvContent = {
    val fileSource = Source.fromFile(dacContentFilePath)
    val csvLines = fileSource.getLines

    val header = csvLines.next().split(",") map (_.replace("\"","")) toList
    val values = csvLines.toList map (_.split(",") map (_.replace("\"","")) )
    val valuesMaps = values map (header zip _ toMap)

    fileSource.close
    CsvContent(fileName, header, valuesMaps)
  }

}
